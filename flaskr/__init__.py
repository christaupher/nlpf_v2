import os, sys

from flask import Flask,render_template, Blueprint,session, url_for, redirect
from flask_dance.contrib.google import make_google_blueprint, google
from oauthlib.oauth2 import OAuth2Error

# You must configure these 3 values from Google APIs console
# console.cloud.google.com/apis/credentials
GOOGLE_CLIENT_ID = '345350618209-4fr0oji2tu4gaj8s2c7krtvl3p64vdt5.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'WqTn4abYLMuy6hoFBJg8A8jV'

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True, template_folder='../templates')
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    bp = make_google_blueprint(
        client_id=GOOGLE_CLIENT_ID,
        client_secret=GOOGLE_CLIENT_SECRET,
        scope=[
            "https://www.googleapis.com/auth/plus.me",
            "https://www.googleapis.com/auth/userinfo.email",
            'https://www.googleapis.com/auth/calendar',
        ],
        redirect_url="/login"
    )
    app.register_blueprint(bp, url_prefix="/")



    # a simple page to try the server
    @app.route('/')
    def index():
        return render_template('index.html')



    @app.route("/login")
    def log():
        creds = None
        if not google.authorized:
            return redirect(url_for("google.login"))
        try:
            resp = google.get("/oauth2/v2/userinfo")
            assert resp.ok, resp.text
            session.clear()
            session['adresse_email'] = resp.json()["email"]

            return redirect(url_for('auth.login'))
        except (InvalidGrantError, ExpiredTokenError) as e: # or maybe any OAuth2Error
            return redirect(url_for("google.login"))

    # you can register your routes files in the blueprint
    from . import auth
    app.register_blueprint(auth.bp)

    from . import client
    app.register_blueprint(client.bp)

    from . import admin
    app.register_blueprint(admin.bp)

    from . import db
    db.init_app(app)

    return app