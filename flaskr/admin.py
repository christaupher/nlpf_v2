#!/usr/bin/python3.5

import functools
import sys
import datetime
import json

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask_dance.contrib.google import google
from flaskr.db import get_db

bp = Blueprint('admin', __name__, url_prefix='/admin')

def is_admin():
    db = get_db()
    user = db.execute(
        'SELECT admin FROM user WHERE adresse_email = ?', (session.get('adresse_email'),)
    ).fetchone()
    return user[0]

def add_tickets():
    db = get_db()
    tickets = db.execute(
                'INSERT INTO tickets (photo, id_batiment, orientation, adresse_email_client, etage, date_intervention, time_intervention, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?)',
                ('', 1, 'nord-ouest', 'client2@nlpf.com', 1, datetime.date(2018, 12, 21), '12:00', 'accepted', 
                '', 2, 'sud', 'client1@nlpf.com', 6, datetime.date(2018, 12, 24), '15:00', 'refused', 
                '', 1, 'sud', 'client2@nlpf.com', 2, datetime.date(2018, 12, 28), '15:00', 'reviewing')
                #('', 2, 'est', 'client1@nlpf.com', 6, '25/12/2018', '15:00', 'refused'),
                #('', 2, 'ouest', 'client1@nlpf.com', 7, '01/01/2019', '15:00', 'reviewing')
                )

    db.execute(
                'INSERT INTO user (nom, prenom, telephone, adresse_email, hash, admin, blacklist) VALUES (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ? , ?)',
                ('Solignac', 'Louis', '060606060', 'louis.solignac@gmail.com', 'toto', True, False,
                'Client2', 'prenom', '060606060', 'client1@nlpf.com', 'toto', False, False,
                'Client3', 'prenom', '060606060', 'client2@nlpf.com', 'toto', False, False,
                'Client4', 'prenom', '060606060', 'client3@nlpf.com', 'toto', False, True)
                )

    db.execute(
                'INSERT INTO batiment (adresse, code_postal, ville, adresse_email_client, nom) VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)',

                ('77 rue guy moquet', 94400, 'Paris', 'client2@nlpf.com', 'EPITA VJ',
                '14-16 rue voltaire', 94400, 'Le Kremlin Bicêtre', 'client2@nlpf.com', 'EPITA VJ', 
                '77 rue guy moquet', 94400, 'Paris', 'client1@nlpf.com', 'Chez Chris')
                )
    db.commit()

def tickets_list():
    db = get_db()
    tickets = db.execute(
        'SELECT *, batiment.nom AS batiment_nom from tickets LEFT JOIN batiment on tickets.id_batiment = batiment.id;'
    ).fetchall()
    print(tickets)
    return tickets



@bp.route('/profil', methods=('GET', 'POST'))
def profile():
    if session.get('adresse_email') is None or is_admin() == 0:
        return render_template('403.html')
    db = get_db()
    user = db.execute(
        'SELECT * FROM user WHERE adresse_email = ?', (session.get('adresse_email'),)
    ).fetchone()
    tickets = db.execute(
        'SELECT *, batiment.nom AS batiment_nom from tickets LEFT JOIN batiment on tickets.id_batiment = batiment.id WHERE status=?;',
        ("accepted",)
    ).fetchall()

    return render_template('admin/starting_page.html', admin = user, tickets=tickets)


@bp.route('/liste-tickets', methods=('GET', 'POST'))
def ticket():
    if session.get('adresse_email') is None or is_admin() == 0:
        return render_template('403.html')
    db = get_db()
    #add_tickets()
    tickets = tickets_list()
    return render_template('admin/liste-ticket.html', tickets = tickets)

@bp.route('/liste-clients', methods=('GET', 'POST'))
def client():
    if session.get('adresse_email') is None or is_admin() == 0:
        return render_template('403.html')
    db = get_db()
    liste_clients =  db.execute(
        'SELECT * from user where admin = 0'
    ).fetchall()
    return render_template('admin/liste-client.html', clients = liste_clients)

@bp.route('/ticket/<id>', methods=('GET', 'POST'))
def ticket_id(id):
    if session.get('adresse_email') is None or is_admin() == 0:
        return render_template('403.html')
    db = get_db()
    ticket = db.execute(
        'SELECT *, batiment.nom AS batiment_nom FROM tickets LEFT JOIN batiment ON batiment.id=tickets.id_batiment LEFT JOIN user ON user.adresse_email=tickets.adresse_email_client where tickets.id=?', (id,)
    ).fetchone()
    if request.method == 'POST' and request.form['btn'] == 'refuse':
        reason = request.form['reason']
        db.execute('UPDATE tickets SET status = ?, reason_rejection = ? WHERE id = ?',
            ('refused', reason, id))
        db.commit()
        return redirect(url_for('admin.profile'))
    if request.method == 'POST' and request.form['btn'] == 'accept':
        date = request.form['date']
        time = request.form['time']
        db.execute('UPDATE tickets SET date_intervention = ?, time_intervention = ?, status = ? WHERE id = ?',
            (date, time, 'accepted', id))
        db.commit()
        return redirect(url_for('admin.profile'))
    if request.method == 'POST' and request.form['btn'] == 'done':
        db.execute('UPDATE tickets SET status = ? WHERE id = ?',
            ('finish', id))
        db.commit()
        return redirect(url_for('admin.profile'))
    if request.method == 'POST' and request.form['btn'] == 'modify':
        db.execute('UPDATE tickets SET status = ? WHERE id = ?',
            ('reviewing', id))
        db.commit()
        return redirect(url_for('admin.profile'))
    return render_template('admin/ticket-detail.html', ticket = ticket)

@bp.route('/blacklist/<adresse_email>', methods=('GET', 'POST'))
def blacklist_user(adresse_email):
    db = get_db()
    ticket = db.execute(
        'UPDATE user SET blacklist = 1 where adresse_email=?', (adresse_email,)
    ).fetchone()
    db.commit()
    liste_clients =  db.execute(
        'SELECT * from user where admin = 0'
    ).fetchall()
    return render_template('admin/liste-client.html', clients = liste_clients)

@bp.route('/unblacklist/<adresse_email>', methods=('GET', 'POST'))
def unblock_user(adresse_email):
    db = get_db()
    ticket = db.execute(
        'UPDATE user SET blacklist = 0 where adresse_email=?', (adresse_email,)
    ).fetchone()
    db.commit()
    liste_clients =  db.execute(
        'SELECT * from user where admin = 0'
    ).fetchall()
    return render_template('admin/liste-client.html', clients = liste_clients)
