import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register', methods=('GET', 'POST'))
def register():
    email = session.get('adresse_email')
    if request.method == 'POST':
        nom = request.form['name']
        prenom = request.form['firstname']
        telephone = request.form['phone']
        db = get_db()
        error = None

        if db.execute(
            'SELECT id FROM user WHERE adresse_email = ?', (email,)
        ).fetchone() is not None:
            error = 'User {} is already registered.'.format(email)

        if error is None:
            db.execute(
                'INSERT INTO user (nom, prenom, telephone, adresse_email, admin) VALUES (?, ?, ?, ?, ?)',
                (nom, prenom, telephone, email, False)
            )
            print(hash)
            db.commit()
            return redirect(url_for('auth.login', email=email))

        flash(error)

    return render_template('auth/register.html', email = email)


@bp.route('/login', methods=('GET', 'POST'))
def login():
    email = session.get('adresse_email')
    print("[AUTH] login : email= " + email)
    db = get_db()
    error = None
    user = db.execute(
        'SELECT * FROM user WHERE adresse_email = ?', (email,)
    ).fetchone()

    if user is None:
        return redirect(url_for('auth.register', email=email))

    if error is None:
        session.clear()
        session['adresse_email'] = user['adresse_email']
        if user['admin']:
            return redirect(url_for('admin.profile'))
        else:
            if user['blacklist'] == 1:
                session.clear()
                return render_template('403.html')
            return redirect(url_for('client.profile'))
    flash(error)

    return render_template('base.html')

@bp.route('/makeAdmin', methods=('GET', 'POST'))
def makeAdmin():
    db = get_db()
    user = db.execute(
        'UPDATE user SET admin=? WHERE adresse_email = ?', (1, session.get('adresse_email'),)
    )
    db.commit()
    return redirect(url_for('admin.profile'))

@bp.route('/makeClient', methods=('GET', 'POST'))
def makeClient():
    db = get_db()
    user = db.execute(
        'UPDATE user SET admin=? WHERE adresse_email = ?', (0, session.get('adresse_email'),)
    )
    db.commit()
    return redirect(url_for('client.profile'))

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view