DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS batiment;
DROP TABLE IF EXISTS tickets;

CREATE TABLE user (
  "id" INTEGER,
  "nom" varchar(128) NOT NULL,
  "prenom" varchar(128) NOT NULL,
  "telephone" VARCHAR(10),
  "adresse_email" varchar(128) NOT NULL unique,
  "outlook_id" varchar(128) unique,
  "admin" boolean,
  "blacklist" boolean,
  "access_token" varchar(2560),

  PRIMARY KEY(adresse_email)
);

CREATE TABLE batiment (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "adresse" varchar(128) NOT NULL,
  "code_postal" int,
  "ville" VARCHAR(128),
  "adresse_email_client" varchar(128) REFERENCES user(adresse_email) ON DELETE CASCADE,
  "nom" varchar(128)
);

CREATE TABLE tickets (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "photo" varchar(128),
  "id_batiment" int REFERENCES batiment(id) ON DELETE CASCADE,
  "orientation" VARCHAR(32) NOT NULL,
  "adresse_email_client" varchar(128) REFERENCES user(adresse_email) ON DELETE CASCADE,
  "etage" int NOT NULL,
  "date_intervention" DATE,
  "time_intervention" TIME,
  "status" VARCHAR(16),
  "description" varchar(128),
  "reason_rejection" varchar(128)
);

INSERT INTO user (nom, prenom, telephone, adresse_email, admin, blacklist) VALUES 
          ('Bricoleur', 'Bob', '060606060', 'bob@nlpf.com',  1, 0)
