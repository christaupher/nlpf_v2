import functools
import os
from werkzeug.utils import secure_filename

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from flaskr.db import get_db

bp = Blueprint('client', __name__, url_prefix='/client')

def is_admin():
    db = get_db()
    user = db.execute(
        'SELECT admin FROM user WHERE adresse_email = ?', (session.get('adresse_email'),)
    ).fetchone()
    return user[0]

def is_blacklisted():
    db = get_db()
    user = db.execute(
        'SELECT blacklist FROM user WHERE adresse_email = ?', (session.get('adresse_email'),)
    ).fetchone()
    return user[0]


@bp.route('/profile', methods=('GET', 'POST'))
def profile():
    if session.get('adresse_email') is None or is_admin() == 1 or is_blacklisted():
        return render_template('403.html')
    db = get_db()
    if request.method == 'POST' and request.form['btn'] == 'ModifyProfile':
        firstname = request.form['firstname']
        name = request.form['name']
        phone = request.form['telephone']
        mail = session.get('adresse_email')
        db.execute(
            'UPDATE user SET prenom = ?, nom = ?, telephone = ? WHERE adresse_email = ?', (firstname, name, phone, mail)
        )
        db.commit()

    elif request.method == 'POST' and request.form['btn'] == 'CreateBatiment':
        name = request.form['name']
        city = request.form['city']
        postcode = request.form['postcode']
        address = request.form['address']
        db.execute(
            'INSERT INTO batiment (adresse, code_postal, ville, adresse_email_client, nom) VALUES ( ?, ?, ?, ?, ?)',
            (address, postcode, city, session.get('adresse_email'), name)
        )
        db.commit()

    elif request.method == 'POST' and request.form['btn'] == 'ModifyBatiment':
        name = request.form['name']
        city = request.form['city']
        postcode = request.form['postcode']
        address = request.form['address']
        id = request.form['id']
        db.execute(
            'UPDATE batiment SET adresse = ?, code_postal = ?, ville = ?, adresse_email_client = ?, nom = ? WHERE id = ?',
            (address, postcode, city, session.get('adresse_email'), name, id)
        )
        db.commit()

    elif request.method == 'POST' and request.form['btn'] == 'DeleteBatiment':
        batimentId = request.form['batimentid']
        db.execute(
            'DELETE from batiment WHERE id = ?', (batimentId,)
        )
        db.commit()
        db.execute(
            'INSERT INTO tickets (id_batiment, orientation, adresse_email_client, etage, date_intervention, time_intervention, status, description, reason_rejection) VALUES ( ?, ?, ?, ?, DATE(?), ?, ?, ?, ?)',
            ('1', 'NORD', session.get('adresse_email'), '4', '2017-01-02', '12:44', 'accepted', 'description', 'nop')
        )
        db.commit()
        db.execute(
            'INSERT INTO tickets (id_batiment, orientation, adresse_email_client, etage, date_intervention, time_intervention, status, description, reason_rejection) VALUES ( ?, ?, ?, ?, DATE(?), ?, ?, ?, ?)',
            ('1', 'NORD', session.get('adresse_email'), '4', '2017-01-02', '12:44', 'done', 'description', 'nop')
        )
        db.commit()

    elif request.method == 'POST' and request.form['btn'] == 'CreateTicket':
        file = request.files['file']
        filename = secure_filename(file.filename)
        file.save(os.path.join(os.path.abspath('flaskr/static/img'), filename))
        db.execute('INSERT INTO tickets (photo, id_batiment, orientation, adresse_email_client, etage, status, description) VALUES (?, ?, ?, ?, ?, ?, ?)',
            (file.filename, request.form['building'], request.form['orientation'], request.form['owner_email'], request.form['etage'], 'reviewing', request.form['description']))
        db.commit()

    user = db.execute(
        'SELECT * FROM user WHERE adresse_email = ?', (session.get('adresse_email'),)
    ).fetchone()
    batiments = db.execute(
        'SELECT * FROM batiment WHERE adresse_email_client = ?', (session.get('adresse_email'),)
    ).fetchall()
    tickets = db.execute(
        'SELECT *, user.nom AS name FROM user LEFT JOIN (SELECT *, tickets.adresse_email_client AS email, batiment.nom AS bat, tickets.id AS ticket FROM tickets LEFT JOIN batiment ON id_batiment = batiment.id) AS ticketbatiment ON user.adresse_email = email WHERE user.adresse_email = ?',
        (session.get('adresse_email'),)
    ).fetchall()
    return render_template('client/client-profile.html', user = user, batiments = batiments, tickets = tickets)

@bp.route('/ticket/<id>', methods=('GET', 'POST'))
def ticket_id(id):
    db = get_db()
    if session.get('adresse_email') is None or is_admin() == 1 or is_blacklisted():
        return render_template('403.html')
    if request.method == 'POST':
        batiment_id = request.form['batiment']
        orientation = request.form['orientation']
        description = request.form['description']
        db.execute('UPDATE tickets SET id_batiment = ?, orientation = ?, description = ?, status = ?, reason_rejection = ? WHERE id = ?',
            (batiment_id, orientation, description, 'reviewing', '', id))
        db.commit()
        return redirect(url_for('client.profile'))
    ticket = db.execute(
        'SELECT *, batiment.nom AS batiment_nom FROM tickets LEFT JOIN batiment ON batiment.id=tickets.id_batiment LEFT JOIN user ON user.adresse_email=tickets.adresse_email_client where tickets.id=?', (id,)
    ).fetchone()
    batiments = db.execute('SELECT * FROM batiment WHERE adresse_email_client = ?', (session.get('adresse_email'),)).fetchall()
    return render_template('client/ticket-detail.html', ticket = ticket, batiments = batiments)
