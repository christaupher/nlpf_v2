#! /bin/sh

export FLASK_APP=flaskr
export FLASK_DEBUG=true
export OAUTHLIB_INSECURE_TRANSPORT=1
export OAUTHLIB_RELAX_TOKEN_SCOPE=1

flask init-db
flask run
