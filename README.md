# SIGL 2019 - NLPF v2 - Bob Super Awesome Website

## About the project

### Goal

## Authors

This application is designed and developped by group from SIGL
2019 class, composed of
the following members:
* D'ESTE Vivien
* HUYNH Quang Nghi
* LY Christopher
* SOLIGNAC Louis-Francois

### Features

This application provides the following features:
* Interface 

### Teachnology used:

* Back-end: Flask (8.11.3)
* Front-End: VueJS (3.3.0)
* Database: SQLite (9.5.10)
* Python 3.2+

#### Init database
The init of the database is done after setting the 2 variables FLASK_APP and FLASK_DEBUG


### Installation

`$ pip3 install Flask` <br>
`$ pip install flask-bootstrap` <br>
`$ pip install Flask-Dootstrap` <br>

#### To run the project

`$ export FLASK_APP=flaskr` <br>
`$ export FLASK_DEBUG=true` <br>
`$ export OAUTHLIB_INSECURE_TRANSPORT=1` <br>
`$ export OAUTHLIB_RELAX_TOKEN_SCOPE=1` <br>
`$ flask init-db` <br>
`$ flask run`

### Login with Google account
Login to NLPF: 
- index page : "Connexion" button
- Google page : Log in with a google account
- Register page : ONLY ONE 1ST TIME - Register your first name, last name, phone
- You are redirected to client profile 

ROUTES ONLY FOR DEV - TO BE DELETED
Change the account to admin
Route /auth/makeAdmin
Change the account to client
Route /auth/makeClient

